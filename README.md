# Structur Project Springboot - Resep Masakan

# Guide Step by step membuat Project Springboot

Berikut merupakan guide atau langkah-langkah bagaimana cara membuat Project Springboot secara lengkap.

## Prerequisite
- Java jdk 17.
- Maven 3.6 keatas.
- PostgreSQL 12 Keatas.
- IDE, IntelliJ, VsCode atau Eclipse.
- Pgadmin atau dbeaver.
- Postman / Thunder Client (Vs Code Extenstions).
- Database Resep Masakan.

Siapkan Environment yang dibutuhkan diatas, Jika belum install bisa cek link berikut ini : https://gitlab.com/ideki11/springboot-set-up-environment

## YouTube Overview

[![SettingEnvironmentYT](https://img.youtube.com/vi/tOAysu_mXLE/0.jpg)](https://www.youtube.com/watch?v=tOAysu_mXLE)

## Step by step
> Guid ini akan dimulai dari pembuatan project dari awal

> Restore database Resep Masakan

1. Buka pgAdmin atau dBeaver.
- pgAdmin, Buka pgAdmin kemudian masukan password postgresql.
- dBeaver, Buat koneksi terlebih dahulu ke server database postgre yang ada di local kita. Seperti Berikut ini:\
	![DbeaverConnnection][def2]

    - New Database Connection
    - Pilih PostgreSQL, kemudian Next.
    - Setting connection menggunakan username dan password dari postgres sql seperti berikut ini: (Jangan lupa untuk ceklis "Show all database")\
		![DbeaverTestConnection][def3]

    - Kemudian klik Test Connection. Jika setting sudah sesuai dan berhasil, maka akan menampilkan Pop up message Connection Test "Connected". Jika hasilnya gagal, maka ada kesalahan dalam setting connection nya.
    - Jika hasil test sudah Connected. Klik Finish.

2. Buat database baru dengan nama "resep-masakan-db".
3. Buka Query Tools, Copy isi file sql latihanresep_backup.sql pada query tools.
	- Contoh Restore pgAdmin:
		![RestoreDbPgAdmin][def11]

		[def11]: img/RestoreDbpgAdmin.PNG
	- Contoh Restore dbeaver:
		![RestoreDbeaver][def12]
		
		[def12]: img/RestoreDbDbeaver.PNG
4. Restore Database Resep Masakan, dengan cara eksekusi query yang ada pada query tools.
5. Jika sudah berhasi di restore, maka untuk schema databasenya adalah seperti berikut ini:\
	![schemaDb][def6]

[def2]: img/DbeaverConnection.PNG
[def3]: img/DbeaverTestConnection.PNG
[def6]: img/SchemaDB.PNG

> Generate Projet Springboot

1. Masuk ke link : https://start.spring.io . Buatlah sebuah Project Springboot dengan configurasi seperti gambar dibawah ini:\
![GenerateProject][def]

2. Dependency yang digunakan saat generate Project adalah:
	- Spring Data JPA
	- Spring Web
	- Lombok
	- Validation
	- Spring Boot DevTools
	- PostgreSQL Driver

3. Klik Generate dan download.

4. Unzip Project Resep Masakan pada drive yang diinginkan di PC/Laptop. Misalkan di drive "d:\resep".

5. Buka Project yang sudah di unzip menggunakan IDE seperti VsCode, Ecplipse, IntelliJ IDE atau yang lainnya.

[def]: img/GenerateProject.PNG

6. Tunggu sampai project selesai di build dan update. Biasanya bisa terlihat statusnya dibawah kanan.

7. Berikut adalah contoh struktur project yang akan kita buat.
	- View Structure project pada VScode.\
		![ViewStructVsCode][def4]
	- View Structure project pada Eclipse.\
		![ViewStructEclipse][def5]

[def4]: img/StrukturProjectVsCode.PNG
[def5]: img/StrukturProjectEcplise.PNG

> Setting Configuration Database di application.properties.

1. Tambahkan konfigurasi database postgre pada file application.properties. Dengan konfigurasi seperti berikut ini:
	```Config
		spring.application.name=Resep Masakan

		## Spring DATASOURCE (DataSourceAutoConfiguration & DataSourceProperties)
		spring.datasource.url = jdbc:postgresql://localhost:5432/resep-masakan-db
		spring.datasource.username = postgres
		spring.datasource.password = secret

		# The SQL dialect makes Hibernate generate better SQL for the chosen database
		spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.PostgreSQLDialect

		# Hibernate ddl auto (create, create-drop, validate, update)
		spring.jpa.hibernate.ddl-auto = validate
		server.port=8080
	```
	- `spring.datasource.url = jdbc:postgresql://localhost:5432/resep-masakan`: Ini adalah URL JDBC untuk koneksi ke database PostgreSQL. `localhost`: `5432` adalah alamat dan port dari server PostgreSQL, sedangkan `/resep-masakan` adalah nama database yang akan digunakan.

	- `spring.datasource.username = postgres dan spring.datasource.password = root`: Ini adalah username dan password yang akan digunakan untuk mengakses database PostgreSQL.

	- `spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.PostgreSQLDialect`: Konfigurasi ini menentukan dialek Hibernate yang harus digunakan oleh aplikasi. Hibernate perlu tahu dialek database yang digunakan agar dapat menghasilkan SQL yang sesuai. Dalam hal ini, Hibernate menggunakan dialek PostgreSQL untuk memastikan SQL yang dihasilkan sesuai dengan spesifikasi PostgreSQL.

	- `spring.jpa.hibernate.ddl-auto = validate`: Parameter ini menentukan perilaku Hibernate saat mengelola skema database. Dalam kasus ini, nilai "validate" dipilih, yang berarti Hibernate hanya akan memvalidasi bahwa skema database sudah ada dan sesuai dengan definisi entitas yang diberikan. Ini berarti Hibernate tidak akan membuat atau mengubah struktur skema secara otomatis. Memilih nilai ini cocok untuk lingkungan produksi di mana struktur database sudah ditentukan dan tidak boleh diubah secara otomatis.

	- `server.port=8080`: Ini menentukan port tempat server aplikasi Spring Boot akan berjalan. Dalam hal ini, server akan berjalan di port 8080. Menentukan port ini penting agar aplikasi dapat diakses melalui jaringan, dan port ini harus dipilih dengan hati-hati untuk menghindari konflik dengan aplikasi lain yang berjalan pada server yang sama.

	Dengan menggabungkan semua konfigurasi ini, aplikasi Spring Boot dapat terhubung dengan database PostgreSQL yang sesuai, menggunakan pengguna dan kata sandi yang tepat, dan menghasilkan SQL yang benar untuk operasi database. Selain itu, aplikasi akan berjalan di port 8080, siap untuk melayani permintaan klien.

> ## Membuat Entity Model.
1. Buatlah sebuah Package atau direktori (Folder) baru dengan nama "models".

2. Buatlah Entity Model dengan nama `Level`.java, Dimana entity tersebut Berdasarkan tabel levels yang ada di database.
	- Berikut adalah Tabel levels pada database, beserta attribute atau colomnya dan juga tipe data yang digunakan.\
		![TabelLevels][def7]
	
	[def7]: img/TabelLevels.PNG
	- Pada class Entity `Level.java`, tuliskan attribute atau properties class berdasarkan tabel levels. Harus diperhatikan bahwa tipe data yang ada pada Entity Model, harus sama dengan tipe data yang ada pada tabel database. Seperti dibawah ini:
	```Lelvel.Java
		public class Level {
				private Integer levelId;
				private String levelName;
				private Date createdTime;
				private Date modifiedTime;

		}
	```
	
	- Kemudian tambahkan annotasi-annotasi yang dibutuhkan sebagai Standard Entity seperti dibawah ini:

	```Level.java
		@Data
		@AllArgsConstructor
		@NoArgsConstructor
		@SuperBuilder
		@Entity
		@Table(name = "levels", schema = "public")
		@EntityListeners(AuditingEntityListener.class)
		@JsonIgnoreProperties(value = {"createdTime", "modifiedTime"}, allowGetters = true)
		public class Level {
			@Id
			@GeneratedValue(strategy = GenerationType.IDENTITY)
			private Integer levelId;

			@Column(name = "level_name", nullable = false)
			private String levelName;

			@Column(nullable = false, updatable = false)
			@Temporal(TemporalType.TIMESTAMP)
			@CreatedDate
			private Date createdTime;

			@Column(nullable = false)
			@Temporal(TemporalType.TIMESTAMP)
			@LastModifiedDate
			private Date modifiedTime;

			@OneToMany(mappedBy = "level", fetch = FetchType.LAZY)
    		private List<Recipe> recipes;

		}
	```
	- Berikut adalah penjelasan annotasi-annotasi yang digunakan pada kodingan Level.java.
		- `@Data`, `@AllArgsConstructor`, `@NoArgsConstructor`, `@SuperBuilder`: Ini adalah anotasi yang kita gunakan dari proyek atau dependency Lombok.\
			@Data: Menghasilkan boilerplate code seperti getter, setter, toString(), equals(), dan hashCode() secara otomatis.
			@AllArgsConstructor: Membuat konstruktor dengan semua parameter.
			@NoArgsConstructor: Membuat konstruktor tanpa parameter.
			@SuperBuilder: Menghasilkan builder untuk kelas ini.
		- `@Entity`: Anotasi ini menandakan bahwa kelas ini adalah sebuah entitas JPA (Java Persistence API), yang akan dipetakan ke sebuah tabel dalam basis data.
		- `@Table(name = "levels", schema = "public")`: Digunakan untuk menentukan nama tabel dan skema di mana entitas akan disimpan. Dalam hal ini, nama tabel adalah "levels" dan skema adalah "public".
		- `@EntityListeners(AuditingEntityListener.class)`: Anotasi ini digunakan untuk menentukan kelas listener yang akan mengatur perubahan entitas. Dalam hal ini, AuditingEntityListener.class akan mengatur entitas ini untuk perubahan waktu pembuatan (createdTime) dan waktu modifikasi (modifiedTime).
		- `@JsonIgnoreProperties(value = {"createdTime", "modifiedTime"}, allowGetters = true)`: Anotasi ini digunakan untuk menentukan properti atau attribute mana yang akan diabaikan ketika proses menggunakan dari dan ke JSON. Dalam hal ini, properti createdTime dan modifiedTime akan diabaikan dalam get dari JSON.
		- `@Id`: Menandakan bahwa field ini adalah primary key dalam tabel.
		- `@GeneratedValue(strategy = GenerationType.IDENTITY)`: Menentukan cara nilai primary key di-generate. GenerationType.IDENTITY berarti bahwa nilai primary key akan di-generate oleh database secara otomatis.
		- `@Column(name = "level_name", nullable = false)`: Menandakan bahwa field ini adalah sebuah kolom dalam tabel dengan nama "level_name". nullable = false menunjukkan bahwa kolom ini tidak boleh memiliki nilai null.
		- `@Temporal(TemporalType.TIMESTAMP)`: Digunakan untuk menentukan tipe data temporal (tanggal dan waktu). Dalam hal ini, digunakan untuk menandai kolom waktu pembuatan dan waktu modifikasi.
		- `@CreatedDate dan @LastModifiedDate`: Anotasi ini menandakan bahwa field tersebut akan diisi otomatis dengan waktu pembuatan entitas dan waktu terakhir entitas diubah.
		- `@OneToMany(mappedBy = "level", fetch = FetchType.LAZY)`: Anotasi ini menandakan bahwa setiap entitas Level akan memiliki banyak entitas Recipe. mappedBy = "level" menunjukkan field dalam entitas Recipe yang digunakan untuk menunjuk kembali ke entitas Level. FetchType.LAZY berarti bahwa entitas Recipe tidak akan dimuat secara otomatis kecuali jika diminta.
	
	- Harus diperhatikan juga ketika menambahkan Anotasi-anotasi tersebut "agar menuliskan import yang benar dan tepat".
	```Import
		import java.util.Date;
		import java.util.List;

		import org.springframework.data.annotation.CreatedDate;
		import org.springframework.data.annotation.LastModifiedDate;
		import org.springframework.data.jpa.domain.support.AuditingEntityListener;

		import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

		import jakarta.persistence.Column;
		import jakarta.persistence.Entity;
		import jakarta.persistence.EntityListeners;
		import jakarta.persistence.FetchType;
		import jakarta.persistence.GeneratedValue;
		import jakarta.persistence.GenerationType;
		import jakarta.persistence.Id;
		import jakarta.persistence.OneToMany;
		import jakarta.persistence.Table;
		import jakarta.persistence.Temporal;
		import jakarta.persistence.TemporalType;
		import lombok.AllArgsConstructor;
		import lombok.Data;
		import lombok.NoArgsConstructor;
		import lombok.experimental.SuperBuilder;
	```
3. Buatlah Entity Model dengan nama `Category`.java, Dimana entity tersebut Berdasarkan tabel categories yang ada di database.
	- Berikut adalah Tabel categories pada database, beserta attribute atau colomnya dan juga tipe data yang digunakan.\
		![TabelCategories][def8]

		[def8]: img/TabelCategories.png

	- Pada class Entity `Category.java`, tuliskan attribute atau properties class berdasarkan tabel categories. Harus diperhatikan bahwa tipe data yang ada pada Entity Model, harus sama dengan tipe data yang ada pada tabel database. Seperti dibawah ini:
	```Category.java
		public class Category{
			private Integer categoryId;
			private String categoryName;
			private Date createdTime;
			private Date modifiedTime;
		}
	```
	- Kemudian tambahkan annotasi-annotasi yang dibutuhkan sebagai Standard Entity seperti dibawah ini:
	```Category.java
		@Data
		@AllArgsConstructor
		@NoArgsConstructor
		@SuperBuilder
		@Entity
		@Table(name = "levels", schema = "public")
		@EntityListeners(AuditingEntityListener.class)
		@JsonIgnoreProperties(value = {"createdTime", "modifiedTime"}, allowGetters = true)
		public class Level {
			@Id
			@GeneratedValue(strategy = GenerationType.IDENTITY)
			private Integer levelId;

			@Column(name = "level_name", nullable = false)
			private String levelName;

			@Column(nullable = false, updatable = false)
			@Temporal(TemporalType.TIMESTAMP)
			@CreatedDate
			private Date createdTime;

			@Column(nullable = false)
			@Temporal(TemporalType.TIMESTAMP)
			@LastModifiedDate
			private Date modifiedTime;

			@OneToMany(fetch = FetchType.LAZY, mappedBy = "category")
    		private Set<Recipe> recipes;
		}
	```
	- Untuk penjelasan annotasi-annotasi yang digunakan pada kodingan Category.java hampir sama dengan yang ada pada Level.java.
	- Import yang digunakan sama seperti yang ada pada Category.java.

4. Buatlah Entity Model dengan nama `Users`.java, Dimana entity tersebut Berdasarkan tabel users yang ada di database.
	- Berikut adalah Tabel users pada database, beserta attribute atau colomnya dan juga tipe data yang digunakan.\
		![TabelUsers][def9]

		[def9]: img/TabelUsers.PNG

	- Pada class Entity `Users.java`, tuliskan attribute atau properties class berdasarkan tabel users. Harus diperhatikan bahwa tipe data yang ada pada Entity Model, harus sama dengan tipe data yang ada pada tabel database. Seperti dibawah ini:
	```Users.java
		public class Users {
			private Integer userId;
			private String username;
			private String password;
			private String fullname;
			private String role;
			private Date createdTime;
			private Date modifiedTime;
		}
	```
	- Kemudian tambahkan annotasi-annotasi yang dibutuhkan sebagai Standard Entity seperti dibawah ini:
	```Users.java
			@Data
			@AllArgsConstructor
			@NoArgsConstructor
			@Builder
			@Entity
			@Table(name = "users")
			@EntityListeners(AuditingEntityListener.class)
			@JsonIgnoreProperties(value = {"createdTime", "modifiedTime"}, allowGetters = true)
			public class Users {
				@Id
				@GeneratedValue(strategy = GenerationType.IDENTITY)
				@Column(name = "user_id")
				private Integer userId;

				@Column(name = "username", nullable = false)
				private String username;

				@Column(name = "password", nullable = false)
				private String password;

				@Column(name = "fullname")
				private String fullname;

				@Column(name = "role")
				private String role;

				@Column(nullable = false, updatable = false)
				@Temporal(TemporalType.TIMESTAMP)
				@CreatedDate
				private Date createdTime;

				@Column(nullable = false)
				@Temporal(TemporalType.TIMESTAMP)
				@LastModifiedDate
				private Date modifiedTime;

				@OneToMany(fetch = FetchType.LAZY,mappedBy = "users")
    			private List<Recipe> recipes;
			}
	```
	- Untuk penjelasan annotasi-annotasi yang digunakan pada kodingan Users.java hampir sama dengan yang ada pada Level.java, dan Category.java.

	- Import yang digunakan sama seperti yang ada pada Category.java dan Level.java.

5. Buatlah Entity Model dengan nama `Recipe`.java, Dimana entity tersebut Berdasarkan tabel recipes yang ada di database.
	- Berikut adalah Tabel recipes pada database, beserta attribute atau colomnya dan juga tipe data yang digunakan.\
		![TabelRecipes][def10]

		[def10]: img/TabelRecipes.PNG

	- Pada class Entity `Recipe.java`, tuliskan attribute atau properties class berdasarkan tabel recipes. Harus diperhatikan bahwa tipe data yang ada pada Entity Model, harus sama dengan tipe data yang ada pada tabel database. Seperti dibawah ini:
		```Recepi.java
		@Data
		@AllArgsConstructor
		@NoArgsConstructor
		@SuperBuilder
		@Entity
		@Table(name = "recipes", schema = "public")
		@EntityListeners(AuditingEntityListener.class)
		@JsonIgnoreProperties(value = {"createdTime", "modifiedTime"}, allowGetters = true)
		public class Recipe {
			@Id
			@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_recipe_id_generator")
			@SequenceGenerator(name = "sequence_recipe_id_generator", sequenceName = "recipes_recipe_id_seq", allocationSize = 1, schema = "public")
			@Column(name = "recipe_id")
			private Integer recipeId;

			@Column(name = "recipe_name", nullable = false)
			private String recipeName;

			@Column(name = "how_to_cook")
			private String howToCook;

			@Column(name = "image_filename")
			private String imageFilename;

			@Column(name = "ingredient")
			private String ingredient;

			@Column(name = "time_cook")
			private Integer timeCook;

			@Column(nullable = false, updatable = false)
			@Temporal(TemporalType.TIMESTAMP)
			@CreatedDate
			private Date createdTime;

			@Column(nullable = false)
			@Temporal(TemporalType.TIMESTAMP)
			@LastModifiedDate
			private Date modifiedTime;

			@ManyToOne
			@JoinColumn(name = "category_id")
			private Category category;

			@ManyToOne()
			@JoinColumn(name = "level_id")
			private Level level;

			@ManyToOne
			@JoinColumn(name = "user_id")
			private Users users;
		}
		```
	- Untuk penjelasan annotasi-annotasi yang digunakan pada kodingan Users.java hampir sama dengan yang ada pada Level.java, dan Category.java. Tapi ada beberapa Annotasi baru yang ditambahkan juga.

	- `@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_recipe_id_generator")`: Ini adalah anotasi yang digunakan untuk menentukan bagaimana nilai dari atribut recipeId akan di-generate oleh Hibernate saat sebuah entitas baru dibuat. Parameter strategy menentukan strategi yang digunakan untuk menghasilkan nilai, dalam hal ini menggunakan GenerationType.SEQUENCE. Parameter generator menentukan nama generator yang digunakan untuk menghasilkan nilai. Nama generator disediakan dalam parameter name pada anotasi @SequenceGenerator.

	- `@SequenceGenerator(name = "sequence_recipe_id_generator", sequenceName = "recipes_recipe_id_seq", allocationSize = 1, schema = "public")`: Ini adalah anotasi yang digunakan untuk mendefinisikan generator urutan (sequence generator) yang akan digunakan untuk menghasilkan nilai untuk atribut recipeId. Parameter name memberikan nama generator yang akan digunakan, yang nantinya akan disebut dalam anotasi @GeneratedValue. Parameter sequenceName menentukan nama urutan di dalam basis data. Parameter allocationSize menentukan jumlah nilai yang akan dialokasikan pada setiap panggilan ke generator urutan. Parameter schema menentukan skema tempat urutan berada di dalam basis data.

	- `@ManyToOne`: Ini adalah anotasi yang digunakan dalam Hibernate untuk menandai hubungan banyak-ke-satu antara dua entitas dalam basis data. Anotasi ini menunjukkan bahwa entitas yang sedang didefinisikan memiliki hubungan banyak-ke-satu dengan entitas lain, dalam hal ini, entitas Category.

	- `@JoinColumn(name = "category_id")`: Anotasi ini digunakan untuk menentukan kolom dalam tabel yang digunakan sebagai foreign key untuk menghubungkan entitas saat ini dengan entitas lain. Dalam kasus ini, parameter name menunjukkan nama kolom dalam tabel yang akan digunakan sebagai foreign key. Kolom ini akan menyimpan nilai identifikasi (ID) dari entitas Category yang terkait dengan entitas saat ini.

	- Import yang digunakan sama seperti yang ada entity lainya seperti pada Category.java dan Level.java.

> ## Membuat Repository

1. Buatlah sebuah Package atau direktori (Folder) baru dengan nama "repositories".

2. Buatlah Repository dengan nama `LevelRepository`.java. Seperti contoh berikut ini, perlu diingat untuk repository merupakan Interface.
	```LevelRepository.java
		package com.example.resep.repositories;

		import org.springframework.data.jpa.repository.JpaRepository;
		import org.springframework.stereotype.Repository;

		import com.example.resep.models.Level;

		@Repository
		public interface LevelRepository extends JpaRepository<Level, Integer> {
			
		}
	```
	- Repository dalam Spring Boot adalah sebuah komponen yang bertanggung jawab untuk berinteraksi dengan databse. Ini adalah bagian dari interface data dalam arsitektur aplikasi Spring Boot dan biasanya digunakan untuk mengakses dan memanipulasi entitas atau objek dalam database.
	
	- `@Repository`: Anotasi ini menandai kelas LevelRepository sebagai sebuah repository Spring. Hal ini memberi tahu Spring bahwa kelas ini adalah bagian dari interface data dalam aplikasi dan akan dikelola oleh konteks aplikasi Spring.

	- `public interface LevelRepository extends JpaRepository<Level, Integer>`: Ini adalah deklarasi interface LevelRepository yang mengimplementasikan interface JpaRepository. Interface JpaRepository memiliki dua parameter jenis: jenis entitas (dalam kasus ini , Level) dan jenis kunci utama (dalam kasus ini, Integer). Dengan mewarisi `JpaRepository`, `LevelRepository` mewarisi metode-metode bawaan untuk melakukan operasi CRUD pada entitas Level, seperti menyimpan entitas, mencari entitas berdasarkan ID, menghapus entitas, dan lain-lain.
	
	- Secara keseluruhan, LevelRepository adalah sebuah repository Spring yang bertanggung jawab untuk melakukan operasi-operasi CRUD pada entitas Level dalam database. Ini dikonfigurasi menggunakan Interface JpaRepository yang menyediakan metode-metode bawaan untuk berinteraksi dengan basis data, dan diberi label dengan anotasi @Repository agar dielus oleh konteks aplikasi Spring.

3. Buatlah Repository dengan nama `CategoryRepository`.java. Seperti contoh berikut ini, perlu diingat untuk repository ini merupakan Interface.
	```CategoryRepository.java
		import org.springframework.data.jpa.repository.JpaRepository;
		import org.springframework.stereotype.Repository;

		import com.example.resep.models.Category;

		@Repository
		public interface CategoryRepository extends JpaRepository<Category, Integer> {
			
		}
	```
4. Buatlah Repository dengan nama `UsersRepository`.java. Seperti contoh berikut ini, perlu diingat untuk repository ini merupakan Interface.
	```UsersRepository.java
		import org.springframework.data.jpa.repository.JpaRepository;
		import org.springframework.stereotype.Repository;

		import com.example.resep.models.Users;

		@Repository
		public interface UsersRepository extends JpaRepository<Users, Integer> {
			
		}
	```

5. Buatlah Repository dengan nama `RecepiRepository`.java. Seperti contoh berikut ini, perlu diingat untuk repository ini merupakan Interface.
	```RecepiRepository.java
		import org.springframework.data.jpa.repository.JpaRepository;
		import org.springframework.stereotype.Repository;

		import com.example.resep.models.Recipe;

		@Repository
		public interface RecipeRepository extends JpaRepository<Recipe, Integer>{
			
		}
	```
> ## Membuat DTO

1. Buatlah sebuah Package atau direktori (Folder) baru dengan nama "dtos".

2. Buatlah DTO dengan nama `LevelDto`.java, dimana DTO dibuat berdasarkan kepada Entitas atau Entity Level.java seperti contoh berikut ini:
	```LevelDto.Java
		@Data
		@AllArgsConstructor
		@NoArgsConstructor
		@SuperBuilder
		public class LevelDto {
			private Integer levelId;
			private String levelName;
			private Date createdTime;
			private Date modifiedTime;
		}
	```
3. Buatlah DTO dengan nama `CategoryDto`.java, dimana DTO dibuat berdasarkan kepada Entitas atau Entity Category.java seperti contoh berikut ini:
	```CategoryDto.Java
		@Data
		@AllArgsConstructor
		@NoArgsConstructor
		@SuperBuilder
		public class CategoryDto {
			private Integer categoryId;
			private String categoryName;
			private Date createdTime;
			private Date modifiedTime;
		}
	```
4. Buatlah DTO dengan nama `UsersDto`.java, dimana DTO dibuat berdasarkan kepada Entitas atau Entity Users.java seperti contoh berikut ini:
	```UsersDto.Java
		@Data
		@AllArgsConstructor
		@NoArgsConstructor
		@SuperBuilder
		public class UsersDto {
			private Integer userId;
			private String username;
			private String fullname;
			private String role;
			private Date createdTime;
			private Date modifiedTime;

		}
	```

5. Buatlah DTO dengan nama `RecipeDto`.java, dimana DTO dibuat berdasarkan kepada Entitas atau Entity Users.java seperti contoh berikut ini:
	```UsersDto.Java
		@Data
		@AllArgsConstructor
		@NoArgsConstructor
		@SuperBuilder
		public class RecipeDto {
			private Integer recipeId;
			private String recipeName;
			private String howToCook;
			private String imageFilename;
			private String ingredient;
			private Integer timeCook;
			private CategoryDto category;
			private LevelDto level;
			private UsersDto users;
			private Date createdTime;
			private Date modifiedTime;
		}
	```
> ## Tambahkan EnableJpaAuditing pada class Main.

1. Buka class ResepMasakanApplication.java, kemudian tambahkan Anotasi @EnableJpaAuditing, seperti contoh berikut ini:\
	```
		import org.springframework.boot.SpringApplication;
		import org.springframework.boot.autoconfigure.SpringBootApplication;
		import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

		@SpringBootApplication
		@EnableJpaAuditing
		public class ResepMasakanApplication {

			public static void main(String[] args) {
				SpringApplication.run(ResepMasakanApplication.class, args);
			}

		}
	```
	- Anotasi `@EnableJpaAuditing` adalah salah satu anotasi khusus yang disediakan oleh Spring Data JPA untuk mengaktifkan fitur auditing di aplikasi Spring Boot yang menggunakan JPA (Java Persistence API). Mari kita bahas lebih detail:
		- Auditing dalam Spring Data JPA: Auditing adalah proses melacak dan mencatat informasi tentang perubahan yang terjadi pada entitas dalam basis data. Dengan menggunakan fitur auditing, kita dapat melacak kapan entitas dibuat dan dimodifikasi, serta siapa yang melakukan perubahan tersebut.
		- Fungsi Anotasi `@EnableJpaAuditing`: Anotasi @EnableJpaAuditing digunakan untuk mengaktifkan fitur auditing dalam aplikasi Spring Boot yang menggunakan JPA. Ketika anotasi ini ditambahkan ke kelas konfigurasi aplikasi, Spring Boot akan secara otomatis mengkonfigurasi aspek-aspek auditing seperti pengisian otomatis tanggal pembuatan `(@CreatedDate)` dan tanggal modifikasi terakhir `(@LastModifiedDate)` pada entitas yang ditandai dengan anotasi tersebut.
		- Cara Penggunaan: Anotasi @EnableJpaAuditing biasanya ditempatkan pada kelas konfigurasi utama aplikasi Spring Boot, seperti kelas dengan anotasi @SpringBootApplication. Dengan menambahkan anotasi ini, fitur auditing akan diaktifkan secara global untuk seluruh entitas dalam aplikasi yang menggunakan JPA.
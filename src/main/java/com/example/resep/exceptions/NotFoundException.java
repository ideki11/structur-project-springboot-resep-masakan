package com.example.resep.exceptions;

public class NotFoundException extends RuntimeException {    
        public NotFoundException(String resourceName, String fieldName, Object fieldValue) {
            super(String.format("%s not found with %s : '%s'", resourceName, fieldName, fieldValue));

        }
}

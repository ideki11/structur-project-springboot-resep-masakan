package com.example.resep.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.example.resep.dtos.ErrorDTO;

@RestControllerAdvice
public class ExceptionHandling {
	
	@ExceptionHandler(NotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ErrorDTO handleNotFoundException(NotFoundException ex) {
		return new ErrorDTO(
				HttpStatus.NOT_FOUND.value(), 
				"application.error.data-not-found.", 
				ex.getMessage()
				);		
	}
}

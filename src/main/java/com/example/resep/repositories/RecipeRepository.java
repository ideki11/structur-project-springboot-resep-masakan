package com.example.resep.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.resep.models.Recipe;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Integer>{
    
}

package com.example.resep.dtos;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UsersDto {
    private Integer userId;
    private String username;
    private String fullname;
    private String role;
    private Date createdTime;
    private Date modifiedTime;
}

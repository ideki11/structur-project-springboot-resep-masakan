package com.example.resep.dtos;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RecipeDto {
    private Integer recipeId;
    private String recipeName;
    private String howToCook;
    private String imageFilename;
    private String ingredient;
    private Integer timeCook;
    private CategoryDto category;
    private LevelDto level;
    private Date createdTime;
    private Date modifiedTime;
}

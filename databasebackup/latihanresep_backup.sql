/*
 Navicat Premium Data Transfer

 Source Server         : postgresql
 Source Server Type    : PostgreSQL
 Source Server Version : 120002
 Source Host           : localhost:5432
 Source Catalog        : resep-masakan
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 120002
 File Encoding         : 65001

*/


-- ----------------------------
-- Sequence structure for users_user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_user_id_seq";
CREATE SEQUENCE "public"."users_user_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

DROP SEQUENCE IF EXISTS "public"."categories_category_id_seq";
CREATE SEQUENCE "public"."categories_category_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

DROP SEQUENCE IF EXISTS "public"."levels_level_id_seq";
CREATE SEQUENCE "public"."levels_level_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

DROP SEQUENCE IF EXISTS "public"."recipes_recipe_id_seq";
CREATE SEQUENCE "public"."recipes_recipe_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS "public"."categories";
CREATE TABLE "public"."categories" (
  "category_id" int4 NOT NULL,
  "category_name" varchar(255) COLLATE "pg_catalog"."default",
  "created_time" timestamp(6),
  "modified_time" timestamp(6)
)
;

-- ----------------------------
-- Table structure for levels
-- ----------------------------
DROP TABLE IF EXISTS "public"."levels";
CREATE TABLE "public"."levels" (
  "level_id" int4 NOT NULL,
  "created_time" timestamp(6),
  "level_name" varchar(255) COLLATE "pg_catalog"."default",
  "modified_time" timestamp(6)
)
;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "user_id" int4 NOT NULL DEFAULT nextval('users_user_id_seq'::regclass),
  "username" varchar(255) COLLATE "pg_catalog"."default",
  "password" text COLLATE "pg_catalog"."default",
  "fullname" varchar(255) COLLATE "pg_catalog"."default",
  "role" varchar(100) COLLATE "pg_catalog"."default",
  "created_time" timestamp(6),
  "modified_time" timestamp(6)
)
;

-- ----------------------------
-- Table structure for recipes
-- ----------------------------
DROP TABLE IF EXISTS "public"."recipes";
CREATE TABLE "public"."recipes" (
  "recipe_id" int4 NOT NULL,
  "created_time" timestamp(6),
  "how_to_cook" text COLLATE "pg_catalog"."default",
  "image_filename" varchar(255) COLLATE "pg_catalog"."default",
  "ingredient" text COLLATE "pg_catalog"."default",
  "modified_time" timestamp(6),
  "recipe_name" varchar(255) COLLATE "pg_catalog"."default",
  "time_cook" int4,
  "category_id" int4,
  "level_id" int4,
  "user_id" int4
)
;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."users_user_id_seq"
OWNED BY "public"."users"."user_id";
SELECT setval('"public"."users_user_id_seq"', 11, false);

ALTER SEQUENCE "public"."categories_category_id_seq"
OWNED BY "public"."categories"."category_id";
SELECT setval('"categories_category_id_seq"', 5, false);

ALTER SEQUENCE "public"."levels_level_id_seq"
OWNED BY "public"."levels"."level_id";
SELECT setval('"levels_level_id_seq"', 5, false);

ALTER SEQUENCE "public"."recipes_recipe_id_seq"
OWNED BY "public"."recipes"."recipe_id";
SELECT setval('"recipes_recipe_id_seq"', 10, false);

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_pk" PRIMARY KEY ("user_id");

-- ----------------------------
-- Primary Key structure for table categories
-- ----------------------------
ALTER TABLE "public"."categories" ADD CONSTRAINT "categories_pkey" PRIMARY KEY ("category_id");

-- ----------------------------
-- Primary Key structure for table levels
-- ----------------------------
ALTER TABLE "public"."levels" ADD CONSTRAINT "levels_pkey" PRIMARY KEY ("level_id");

-- ----------------------------
-- Primary Key structure for table recipes
-- ----------------------------
ALTER TABLE "public"."recipes" ADD CONSTRAINT "recipes_pkey" PRIMARY KEY ("recipe_id");

-- ----------------------------
-- Foreign Keys structure for table recipes
-- ----------------------------
ALTER TABLE "public"."recipes" ADD CONSTRAINT "fk6wb50tfk9lq1am8j9gl69pec1" FOREIGN KEY ("category_id") REFERENCES "public"."categories" ("category_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."recipes" ADD CONSTRAINT "fksr8k7rswbg9fowf42uson9816" FOREIGN KEY ("level_id") REFERENCES "public"."levels" ("level_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."recipes" ADD CONSTRAINT "recipes_users_fk" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;


-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO "public"."categories" VALUES (1, 'Lunch', '2024-03-04 10:08:35', '2024-03-04 10:08:35');
INSERT INTO "public"."categories" VALUES (2, 'Breakfast', '2024-03-04 10:08:35', '2024-03-04 10:08:35');
INSERT INTO "public"."categories" VALUES (3, 'Dinner', '2024-03-04 10:08:35', '2024-03-04 10:08:35');
INSERT INTO "public"."categories" VALUES (4, 'Snack', '2024-03-04 10:08:35', '2024-03-04 10:08:35');

-- ----------------------------
-- Records of levels
-- ----------------------------
INSERT INTO "public"."levels" VALUES (1, '2024-03-04 10:08:35', 'Master Chef', '2024-03-04 10:08:35');
INSERT INTO "public"."levels" VALUES (2, '2024-03-04 10:08:35', 'Hard', '2024-03-04 10:08:35');
INSERT INTO "public"."levels" VALUES (3, '2024-03-04 10:08:35', 'Medium', '2024-03-04 10:08:35');
INSERT INTO "public"."levels" VALUES (4, '2024-03-04 10:08:35', 'Easy', '2024-03-04 10:08:35');

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "public"."users" VALUES (1, 'user1', '$2b$10$9PQ4l3CIhGck3nWCRmEYieQwNCzVw9j7UW86TybdQFe.1Ztwi3G1C', 'Jane Smith', 'User', '2024-03-04 10:08:35', '2024-03-04 10:08:35');
INSERT INTO "public"."users" VALUES (2, 'user2', '$2b$10$9PQ4l3CIhGck3nWCRmEYieQwNCzVw9j7UW86TybdQFe.1Ztwi3G1C', 'Ashley Johnson', 'User', '2024-03-04 10:08:35', '2024-03-04 10:08:35');
INSERT INTO "public"."users" VALUES (3, 'user3', '$2b$10$9PQ4l3CIhGck3nWCRmEYieQwNCzVw9j7UW86TybdQFe.1Ztwi3G1C', 'Bob Brown', 'Admin', '2024-03-04 10:08:35', '2024-03-04 10:08:35');
INSERT INTO "public"."users" VALUES (4, 'user4', '$2b$10$9PQ4l3CIhGck3nWCRmEYieQwNCzVw9j7UW86TybdQFe.1Ztwi3G1C', 'John Doe', 'Admin', '2024-03-04 10:08:35', '2024-03-04 10:08:35');
INSERT INTO "public"."users" VALUES (5, 'user5', '$2b$10$9PQ4l3CIhGck3nWCRmEYieQwNCzVw9j7UW86TybdQFe.1Ztwi3G1C', 'Jane Smith', 'User', '2024-03-04 10:08:35', '2024-03-04 10:08:35');
INSERT INTO "public"."users" VALUES (6, 'user6', '$2b$10$9PQ4l3CIhGck3nWCRmEYieQwNCzVw9j7UW86TybdQFe.1Ztwi3G1C', 'Kukun Kurniawan', 'Admin', '2024-03-04 10:08:35', '2024-03-04 10:08:35');
INSERT INTO "public"."users" VALUES (7, 'user7', '$2b$10$9PQ4l3CIhGck3nWCRmEYieQwNCzVw9j7UW86TybdQFe.1Ztwi3G1C', 'Kukun Kurniawan', 'User', '2024-03-04 10:08:35', '2024-03-04 10:08:35');
INSERT INTO "public"."users" VALUES (8, 'user8', '$2b$10$9PQ4l3CIhGck3nWCRmEYieQwNCzVw9j7UW86TybdQFe.1Ztwi3G1C', 'John Doe', 'Admin', '2024-03-04 10:08:35', '2024-03-04 10:08:35');
INSERT INTO "public"."users" VALUES (9, 'user9', '$2b$10$9PQ4l3CIhGck3nWCRmEYieQwNCzVw9j7UW86TybdQFe.1Ztwi3G1C', 'John Doe', 'User', '2024-03-04 10:08:35', '2024-03-04 10:08:35');
INSERT INTO "public"."users" VALUES (10, 'user10', '$2b$10$9PQ4l3CIhGck3nWCRmEYieQwNCzVw9j7UW86TybdQFe.1Ztwi3G1C', 'Jane Smith', 'Admin', '2024-03-04 10:08:35', '2024-03-04 10:08:35');

-- ----------------------------
-- Records of recipes
-- ----------------------------
INSERT INTO "public"."recipes" VALUES (2, '2024-03-21 10:08:35', 'campur susu cair dan air lemon. Diamkan 3 menit Campur semua bahan kering, aduk rata', 'https://www.allrecipes.com/thmb/imrP1HYi5pu7j1en1_TI-Kcnzt4=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/20513-classic-waffles-mfs-025-4x3-81c0f0ace44d480ca69dd5f2c949731a.jpg', 'tepung terigu serbaguna', '2024-03-21 10:08:35', 'Es Blewah cendol dawet agar-agar', 20, 2, 4, 2);
INSERT INTO "public"."recipes" VALUES (3, '2024-03-21 10:08:35', 'Rebus telur, kentang dan wortel', 'https://www.culinaryhill.com/wp-content/uploads/2022/05/Egg-Salad-Culinary-Hill-1200x800-1.jpg', 'telur bebek rebus', '2024-03-21 10:08:35', 'Egg Salad', 30, 1, 3, 2);
INSERT INTO "public"."recipes" VALUES (4, '2024-03-21 10:08:35', 'Tepung Pisang Goreng Gluten Free Ladang Lima, bubuk minuman coklat dan margarin.', 'https://www.imperialsugar.com/sites/default/files/recipe/Chocolate-Pancakes-imperial.jpg', '200 gram tepung pisang goreng Ladang Lima', '2024-03-21 10:08:35', 'Chocola Pancake', 40, 4, 3, 1);
INSERT INTO "public"."recipes" VALUES (5, '2024-03-21 10:08:35', 'Kocok lepas telur, tambahkan garam, merica, kaldu bubuk secukupnya, kocok rata.', 'https://img.buzzfeed.com/thumbnailer-prod-us-east-1/d1cb1ef69a1c4b6c9a10e1662082f0e7/BFV77395_HowToCookAPerfectOmelet_ADB_032221_V06l_16x9_YT.jpg?resize=1200:*&output-format=jpg&output-quality=auto', '3 butir telur', '2024-03-21 10:08:35', 'Omelette rush', 45, 3, 2, 4);
INSERT INTO "public"."recipes" VALUES (6, '2024-03-28 11:37:25.701695', '<ul><li>Cuci</li><li>Rebus</li><li>Beri sedikit garam</li></ul>', 'https://umsu.ac.id/health/wp-content/uploads/2024/02/memahami-manfaat-luar-biasa-pakcoy-untuk-kesehatan.jpg', '<ul><li>Pakcoy</li><li>Garam</li></ul>', '2024-03-28 11:37:25.701695', 'Pakcoy', 10, 4, 4, 5);
INSERT INTO "public"."recipes" VALUES (8, '2024-04-01 11:40:34.324635', '<ol><li>Siapkan roti, olesi roti dg margarin</li><li>Siapkan grill/teflon utk memanggang beef dan roti</li><li>Potong tipis² timun dan tomat. Dan bersih kan selada</li><li>Bahan saos : campurkan semua bahan saos jd satu dlm wadah</li><li>Setelah beef dan roti matang, siapkan di wadah, tata, mulai dari roti bagian bawah, lalu beri selada, beef, bahan saos, timun dan tomat, terakhir tut</li><li>Dirasa kurang pedas boleh ditambah dg saos lagi</li><li>Beef burger siap di nikmati</li></ol>', 'https://www.foodandwine.com/thmb/DI29Houjc_ccAtFKly0BbVsusHc=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/crispy-comte-cheesburgers-FT-RECIPE0921-6166c6552b7148e8a8561f7765ddf20b.jpg', '<ol><li>🍔&nbsp;<a href="https://cookpad.com/id/cari/Roti" rel="noopener noreferrer" target="_blank" style="color: inherit;">Roti</a>&nbsp;burger</li><li><a href="https://cookpad.com/id/cari/Beef" rel="noopener noreferrer" target="_blank" style="color: inherit;">Beef</a>&nbsp;burger</li><li>🥒<a href="https://cookpad.com/id/cari/Timun" rel="noopener noreferrer" target="_blank" style="color: inherit;">Timun</a></li><li><a href="https://cookpad.com/id/cari/Roti" rel="noopener noreferrer" target="_blank" style="color: inherit;">🍅&nbsp;</a><a href="https://cookpad.com/id/cari/Tomat" rel="noopener noreferrer" target="_blank" style="color: inherit;">Tomat</a></li><li><a href="https://cookpad.com/id/cari/Selada" rel="noopener noreferrer" target="_blank" style="color: inherit;">Selada</a></li><li><a href="https://cookpad.com/id/cari/Margarine" rel="noopener noreferrer" target="_blank" style="color: inherit;">Margarine</a></li></ol><p>Bahan saos</p><ol><li>Mayones</li><li><a href="https://cookpad.com/id/cari/Saos%20tomat" rel="noopener noreferrer" target="_blank" style="color: inherit;">Saos tomat</a>&nbsp;(secukupnya)</li><li><a href="https://cookpad.com/id/cari/Saos%20sambal" rel="noopener noreferrer" target="_blank" style="color: inherit;">Saos sambal</a>&nbsp;(secukupnya)</li></ol>', '2024-04-01 11:40:34.324635', 'Beef Burger', 65, 1, 3, 6);
INSERT INTO "public"."recipes" VALUES (9, '2024-04-01 15:22:14.724293', '1. Masak nasi; 2. Tunggu beberapa menit', 'https://asset.kompas.com/crops/Kyp-MBp3Kf0PLGveth_zzhU2gfI=/0x0:1000x667/750x500/data/photo/2020/07/11/5f09e008e7fee.jpg', 'Nasi; Air; Daun salam; bawang', '2024-04-01 15:22:14.724293', 'Bubur Ayam', 60, 2, 4, 6);
INSERT INTO "public"."recipes" VALUES (7, '2024-03-28 11:40:00.891785', '', 'https://www.foodandwine.com/thmb/DI29Houjc_ccAtFKly0BbVsusHc=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/crispy-comte-cheesburgers-FT-RECIPE0921-6166c6552b7148e8a8561f7765ddf20b.jpg', '', '2024-03-28 11:40:00.891785', 'Jamur', 25, 2, 1, 7);


